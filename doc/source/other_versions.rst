===============================
Docs for other Sardana versions
===============================

The `default sardana docs <http://sardana-controls.org>`_ reflect the latest tagged
version in the repository.

Docs are also built as artifacts by the CI pipeline for each ref (branch or tag)
pushed to the repository, and they can be browsed as well using the following
URL change (REF) by the branch or tag name for which you want to see the docs:

`https://gitlab.com/sardana-org/sardana/-/jobs/artifacts/REF/file/public/index.html?job=build-doc`

For example, to check the docs for the latest push to `develop`, visit:

https://gitlab.com/sardana-org/sardana/-/jobs/artifacts/develop/file/public/index.html?job=build-doc

Note that the docs are also automatically built by the pipelines triggered by
Merge Requests. They can be viewed using the "View App" button in the given
Merge Request page.